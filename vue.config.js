module.exports = {
  transpileDependencies: [
    'vuetify'
  ],

  pluginOptions: {
    i18n: {
      locale: 'hu',
      fallbackLocale: 'hu',
      localeDir: 'locales',
      enableInSFC: true
    }
  },

  pwa: {
    workboxOptions: {
      exclude: [/\.map$/, /_redirects/]
    }
  }
}
