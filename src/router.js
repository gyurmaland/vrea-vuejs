import Vue from 'vue'
import Router from 'vue-router'
import { store } from './store'
import Home from './views/Home.vue'
import Profile from './views/user/Profile.vue'
import Login from './views/auth/Login'
import Workers from './views/worker/Workers'
import WorkRequest from './views/work/WorkRequest'
import WorkerDetail from './views/worker/detail/WorkerDetail'
import Notifications from './views/notification/Notifications'
import Review from './views/review/Review'
import ReviewDetail from './views/review/detail/ReviewDetail'
import Chat from './views/chat/Chat'
import AddInterval from './views/interval/AddInterval'
import WorkRequestList from './views/user/WorkRequestList'
import IncomingWorkRequestListTab from './views/work/IncomingWorkRequestListTab'
import OutgoingWorkRequestListTab from './views/work/OutgoingWorkRequestListTab'
import WorkRequestDetail from '@/views/work/detail/WorkRequestDetail'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/oauth2/redirect',
      name: 'authredirect',
      component: Login
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '/work-requests',
      name: 'workRequests',
      component: WorkRequestList,
      props: true,
      children: [
        {
          path: 'incoming',
          name: 'incomingWorkRequestListTab',
          component: IncomingWorkRequestListTab
        },
        {
          path: 'outgoing',
          name: 'outgoingWorkRequestListTab',
          component: OutgoingWorkRequestListTab
        }
      ]
    },
    {
      path: '/workers',
      name: 'workers',
      component: Workers
    },
    {
      path: '/worker/:workerId/work-request',
      name: 'workRequest',
      component: WorkRequest
    },
    {
      path: '/work-request/:workId',
      name: 'workRequestDetail',
      component: WorkRequestDetail
    },
    {
      path: '/worker/:workerId',
      name: 'workerDetail',
      component: WorkerDetail
    },
    {
      path: '/notifications',
      name: 'notifications',
      component: Notifications
    },
    {
      path: '/review/add/:workId',
      name: 'review',
      component: Review
    },
    {
      path: '/review/:workId',
      name: 'reviewDetail',
      component: ReviewDetail
    },
    {
      path: '/chat/:workId',
      name: 'chat',
      component: Chat
    },
    {
      path: '/add-working-hours',
      name: 'addWorkingHours',
      component: AddInterval
    },
    {
      path: '*',
      component: Home
    }
  ]
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/']
  const regexPublicPage = new RegExp('/oauth2/redirect*')
  const loggedIn = store.state.user.token !== ''
  const authRegex = !regexPublicPage.test(to.path)

  const authRequired = !publicPages.includes(to.path)

  if (authRequired && authRegex && !loggedIn) {
    return next('/login')
  }

  if (to.path === '/login' && loggedIn) {
    return next('/')
  }

  next()
})

export default router
