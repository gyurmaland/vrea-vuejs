import axios from 'axios'
import router from '../router'
import { store } from '../store'

export default () => {
  const axiosStuff = axios.create({
    baseURL: process.env.NODE_ENV !== 'production' ? 'http://localhost:8080/' : 'https://vrea.herokuapp.com/',
    withCredentials: false,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + store.state.user.token
    }
  })

  axiosStuff.interceptors.response.use((response) => {
    return response
  }, function (error) {
    if (error.response.status === 401) {
      store.dispatch('logoutUser', {})
      router.push('/login')
    }
    return Promise.reject(error)
  })

  return axiosStuff
}
