import Vue from 'vue'
import Dayjs from 'vue-dayjs'
import * as VueGoogleMaps from 'vue2-google-maps'
import AOS from 'aos'
import 'aos/dist/aos.css'
import VueChatScroll from 'vue-chat-scroll'
import VueCountdown from '@chenfengyuan/vue-countdown'
import App from './App.vue'
import router from './router'
import { store } from './store'
import './registerServiceWorker'
import vuetify from './plugins/vuetify'
import consts from './plugins/constsPlugin'
import i18n from './i18n'

Vue.config.productionTip = false

Vue.use(consts)

// TODO: a verzió egy tempo fix, később majd package-et kell updatelni
Vue.use(VueGoogleMaps, {
  load: {
    key: consts.GOOGLE_MAP_API_KEY,
    libraries: 'places',
    v: 3.38
  }
})

Vue.use(Dayjs)

Vue.use(VueChatScroll)

Vue.component(VueCountdown.name, VueCountdown)

new Vue({
  router,
  store,
  vuetify,
  i18n,
  created () {
    AOS.init()
  },
  render: h => h(App)
}).$mount('#app')
