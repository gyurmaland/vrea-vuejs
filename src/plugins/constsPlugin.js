const consts = {
  API_BASE_URL: process.env.NODE_ENV !== 'production' ? 'http://localhost:8080/' : 'https://vrea.herokuapp.com/',
  ACCESS_TOKEN: 'accessToken',

  OAUTH2_REDIRECT_URI: process.env.NODE_ENV !== 'production' ? 'http://localhost:8081/oauth2/redirect' : 'https://vrea.netlify.com/oauth2/redirect',

  GOOGLE_AUTH_URL: process.env.NODE_ENV !== 'production' ? 'http://localhost:8080/oauth2/authorize/google?redirect_uri=http://localhost:8081/oauth2/redirect' : 'https://vrea.herokuapp.com/oauth2/authorize/google?redirect_uri=https://vrea.netlify.com/oauth2/redirect',
  FACEBOOK_AUTH_URL: process.env.NODE_ENV !== 'production' ? 'http://localhost:8080/oauth2/authorize/facebook?redirect_uri=http://localhost:8081/oauth2/redirect' : 'https://vrea.herokuapp.com/oauth2/authorize/facebook?redirect_uri=https://vrea.netlify.com/oauth2/redirect',
  GITHUB_AUTH_URL: process.env.NODE_ENV !== 'production' ? 'http://localhost:8080/oauth2/authorize/github?redirect_uri=http://localhost:8081/oauth2/redirect' : 'https://vrea.herokuapp.com/oauth2/authorize/github?redirect_uri=https://vrea.netlify.com/oauth2/redirect',

  GOOGLE_MAP_API_KEY: 'AIzaSyA9UvhLH-h9Apc2hfep8EvAbrqmb8fv2vU'

}

consts.install = function (Vue, options) {
  Vue.prototype.$getConst = (key) => {
    return consts[key]
  }
}

export default consts
