import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import user from './store/modules/user'
import worker from './store/modules/worker'
import workerSearch from './store/modules/workerSearch'
import workRequest from './store/modules/workRequest'
import workRequestSearch from './store/modules/workRequestSearch'
import alert from './store/modules/alert'
import alertStash from './store/modules/alertStash'
import review from './store/modules/review'
import message from './store/modules/message'
import notification from './store/modules/notification'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    user,
    worker,
    workerSearch,
    workRequest,
    workRequestSearch,
    alert,
    alertStash,
    review,
    message,
    notification
  },
  plugins: [createPersistedState()]
})
