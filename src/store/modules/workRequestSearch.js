import i18n from '../../i18n'

const getDefaultState = () => {
  return {
    workStatusType: {
      name: 'WORK.STATUS.PENDING',
      value: 'PENDING'
    },
    workStatusTypes: [
      {
        name: 'WORK.STATUS.PENDING',
        value: 'PENDING'
      },
      {
        name: 'WORK.STATUS.WORK_ACCEPTED',
        value: 'WORK_ACCEPTED'
      },
      {
        name: 'WORK.STATUS.WORK_DONE',
        value: 'WORK_DONE'
      },
      {
        name: 'WORK.STATUS.WORK_DONE_AND_REVIEWED',
        value: 'WORK_DONE_AND_REVIEWED'
      },
      {
        name: 'WORK.STATUS.WORK_DENIED',
        value: 'WORK_DENIED'
      },
      {
        name: 'WORK.STATUS.WORK_CLOSED',
        value: 'WORK_CLOSED'
      }
    ]
  }
}

// initial state
const state = getDefaultState()

const getters = {
  getTranslatedWorkStatusTypes: (state) => {
    return state.workStatusTypes.map(pt => ({ ...pt, name: i18n.t(pt.name) }))
  }
}

const mutations = {
  SET_WORK_STATUS_TYPE (state, workStatusType) {
    state.workStatusType = workStatusType
  }
}

const actions = {
  setWorkStatusType ({ commit }, workStatusType) {
    commit('SET_WORK_STATUS_TYPE', workStatusType)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
