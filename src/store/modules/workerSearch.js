import i18n from '../../i18n'

const getDefaultState = () => {
  return {
    searchSkill: null,
    searchSkills: [
      {
        name: i18n.t('workers.filter.skill.all'),
        value: null
      },
      {
        name: 'GARDENING',
        value: 'GARDENING'
      },
      {
        name: 'BABYSITTING',
        value: 'BABYSITTING'
      },
      {
        name: 'DRIVING',
        value: 'DRIVING'
      }
    ],
    searchDate: '',
    searchOrderBy: {
      title: 'workers.orderBy.default',
      value: 'ID'
    },
    searchOrderByLocation: {
      lat: null,
      lng: null
    }
  }
}

// initial state
const state = getDefaultState()

const getters = {
  getTranslatedSearchSkills: (state) => {
    return state.searchSkills.map(s => ({ ...s, name: i18n.t(s.name) }))
  }
}

const mutations = {
  SET_SEARCH_SKILL (state, searchSkill) {
    state.searchSkill = searchSkill
  },
  SET_SEARCH_DATE (state, searchDate) {
    state.searchDate = searchDate
  },
  SET_SEARCH_ORDER_BY (state, searchOrderBy) {
    state.searchOrderBy = searchOrderBy
  },
  SET_SEARCH_ORDER_BY_LOCATION_LATITUDE (state, latitude) {
    state.searchOrderByLocation.lat = latitude
  },
  SET_SEARCH_ORDER_BY_LOCATION_LONGITUDE (state, longitude) {
    state.searchOrderByLocation.lng = longitude
  }
}

const actions = {
  setSearchSkill ({ commit }, searchSkill) {
    commit('SET_SEARCH_SKILL', searchSkill)
  },
  setSearchDate ({ commit }, searchDate) {
    commit('SET_SEARCH_DATE', searchDate)
  },
  setSearchOrderBy ({ commit }, searchOrderBy) {
    commit('SET_SEARCH_ORDER_BY', searchOrderBy)
  },
  setSearchOrderByLocation ({ commit }, searchOrderByLocation) {
    commit('SET_SEARCH_ORDER_BY_LOCATION_LATITUDE', searchOrderByLocation.latitude)
    commit('SET_SEARCH_ORDER_BY_LOCATION_LONGITUDE', searchOrderByLocation.longitude)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
