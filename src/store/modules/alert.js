const getDefaultState = () => {
  return {
    isVisible: false,
    color: null,
    alertMsg: null,
    alertPayload: {
      notificationType: null,
      notificationPayloadObject: null
    }
  }
}

// initial state
const state = getDefaultState()

const mutations = {
  SET_IS_VISIBLE (state, isVisible) {
    state.isVisible = isVisible
  },
  SET_COLOR (state, color) {
    state.color = color
  },
  SET_ALERT_MSG (state, alertMsg) {
    state.alertMsg = alertMsg
  },
  SET_ALERT_PAYLOAD (state, alertPayload) {
    state.alertPayload = alertPayload
  },
  RESET_ALERT (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  setIsVisible ({ commit }, isVisible) {
    setTimeout(() => {
      commit('RESET_ALERT')
    }, 8000)
    commit('SET_IS_VISIBLE', isVisible)
  },
  setColor ({ commit }, color) {
    commit('SET_COLOR', color)
  },
  setAlertMsg ({ commit }, alertMsg) {
    commit('SET_ALERT_MSG', alertMsg)
  },
  setAlertPayload ({ commit }, alertPayload) {
    commit('SET_ALERT_PAYLOAD', alertPayload)
  },
  resetAlertState ({ commit }) {
    commit('RESET_ALERT')
  }
}

export default {
  state,
  mutations,
  actions
}
