import api from '../../service/api'

const getDefaultState = () => {
  return {
    selectedWorkerId: '',
    selectedSkill: null,
    selectedIntervalId: '',
    title: '',
    starterMsg: ''
  }
}

// initial state
const state = getDefaultState()

const getters = {
  getSelectedWorkerId: (state) => {
    return state.selectedWorkerId
  }
}

const mutations = {
  SET_SELECTED_WORKER_ID (state, selectedWorkerId) {
    state.selectedWorkerId = selectedWorkerId
  },
  SET_SELECTED_SKILL (state, selectedSkill) {
    state.selectedSkill = selectedSkill
  },
  SET_SELECTED_INTERVAL_ID (state, selectedIntervalId) {
    state.selectedIntervalId = selectedIntervalId
  },
  SET_TITLE (state, title) {
    state.title = title
  },
  SET_STARTER_MSG (state, starterMsg) {
    state.starterMsg = starterMsg
  },
  RESET_WORK_REQUEST (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  setSelectedWorkerId ({ commit }, selectedWorkerId) {
    commit('SET_SELECTED_WORKER_ID', selectedWorkerId)
  },
  setSelectedSkill ({ commit }, selectedSkill) {
    commit('SET_SELECTED_SKILL', selectedSkill)
  },
  setSelectedIntervalId ({ commit }, selectedIntervalId) {
    commit('SET_SELECTED_INTERVAL_ID', selectedIntervalId)
  },
  setTitle ({ commit }, title) {
    commit('SET_TITLE', title)
  },
  setStarterMsg ({ commit }, starterMsg) {
    commit('SET_STARTER_MSG', starterMsg)
  },
  resetWorkRequest ({ commit }, accepted) {
    commit('RESET_WORK_REQUEST', accepted)
  },
  findWorkRequestById ({ commit }, workId) {
    return api().get('/work/' + workId)
  },
  saveWorkRequest ({ commit, state }) {
    return api().post('/work/save-work', {
      title: state.title,
      requiredSkill: state.selectedSkill,
      dateTimeIntervalId: state.selectedIntervalId,
      startMsg: state.starterMsg,
      workerId: state.selectedWorkerId
    })
  },
  findIncomingWorkRequests ({ commit }, searchData) {
    return api().post('/work/list', {
      pagingRequest: {
        pageSize: searchData.pagingRequest.pageSize,
        page: searchData.pagingRequest.page
      },
      workSearchRequest: {
        workStatus: searchData.workSearchRequest.workStatus,
        workDirection: searchData.workSearchRequest.workDirection
      }
    })
  },
  findOutgoingWorkRequests ({ commit }, searchData) {
    return api().post('/work/list', {
      pagingRequest: {
        pageSize: searchData.pagingRequest.pageSize,
        page: searchData.pagingRequest.page
      },
      workSearchRequest: {
        workStatus: searchData.workSearchRequest.workStatus,
        workDirection: searchData.workSearchRequest.workDirection
      }
    })
  },
  getWorkDoneCountByWorkerId ({ commit }, workerId) {
    return api().get('/work/work-done-count/' + workerId)
  },
  acceptWorkRequest ({ commit }, acceptData) {
    return api().post('/work/set-work-acceptance', {
      accepted: acceptData.accepted,
      workId: acceptData.workId
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
