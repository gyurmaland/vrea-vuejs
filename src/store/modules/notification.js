import api from '../../service/api'

const getDefaultState = () => {
  return {
  }
}

// initial state
const state = getDefaultState()

const mutations = {
}

const actions = {
  getUserNotifications ({ commit }) {
    return api().get('/notifications/list')
  },
  setUserNotificationToSeenById ({ commit }, notificationId) {
    return api().put('/notification-seen/' + notificationId)
  }
}

export default {
  state,
  mutations,
  actions
}
