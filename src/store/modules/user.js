import api from '../../service/api'
import i18n from '../../i18n'

const getDefaultState = () => {
  return {
    name: '',
    email: '',
    imageUrl: '',
    token: '',
    tokenExpiryDate: null,
    birthDate: null,
    highestEducation: null,
    job: '',
    profileDescription: '',
    selectedSkills: null,
    highestEducations: [
      {
        name: 'ELEMENTARY',
        value: 'ELEMENTARY'
      },
      {
        name: 'MIDDLE',
        value: 'MIDDLE'
      }
    ],
    skills: [
      {
        name: 'GARDENING',
        value: 'GARDENING'
      },
      {
        name: 'BABYSITTING',
        value: 'BABYSITTING'
      },
      {
        name: 'DRIVING',
        value: 'DRIVING'
      }
    ],
    location: {
      latitude: null,
      longitude: null
    },
    googlePlaceId: '',
    googlePlaceName: '',
    intervals: [],
    currentLocation: {
      latitude: null,
      longitude: null
    },
    notificationsCount: 0
  }
}

// initial state
const state = getDefaultState()

const getters = {
  getTranslatedSkills: (state) => {
    return state.skills.map(s => ({ ...s, name: i18n.t(s.name) }))
  },
  getTranslatedHighestEducations: (state) => {
    return state.highestEducations.map(s => ({ ...s, name: i18n.t(s.name) }))
  }
}

const mutations = {
  SET_NAME (state, name) {
    state.name = name
  },
  SET_EMAIL (state, email) {
    state.email = email
  },
  SET_IMAGE_URL (state, imageUrl) {
    state.imageUrl = imageUrl
  },
  SET_TOKEN (state, token) {
    state.token = token
  },
  SET_TOKEN_EXPIRY_DATE (state, tokenExpiryDate) {
    state.tokenExpiryDate = tokenExpiryDate
  },
  SET_BIRTH_DATE (state, birthDate) {
    state.birthDate = birthDate
  },
  SET_HIGHEST_EDUCATION (state, highestEducation) {
    state.highestEducation = highestEducation
  },
  SET_JOB (state, job) {
    state.job = job
  },
  SET_PROFILE_DESCRIPTION (state, profileDescription) {
    state.profileDescription = profileDescription
  },
  SET_SELECTED_SKILLS (state, selectedSkills) {
    state.selectedSkills = selectedSkills
  },
  SET_LOCATION (state, location) {
    state.location = location
  },
  SET_GOOGLE_PLACE_ID (state, googlePlaceId) {
    state.googlePlaceId = googlePlaceId
  },
  SET_GOOGLE_PLACE_NAME (state, googlePlaceName) {
    state.googlePlaceName = googlePlaceName
  },
  SET_INTERVALS (state, intervals) {
    state.intervals = intervals
  },
  SET_CURRENT_LOCATION (state, currentLocation) {
    state.currentLocation = currentLocation
  },
  SET_NOTIFICATIONS_COUNT (state, notificationsCount) {
    state.notificationsCount = notificationsCount
  },
  RESET_USER_STATE (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  setToken ({ commit }, token) {
    commit('SET_TOKEN', token)
    commit('SET_TOKEN_EXPIRY_DATE', new Date(new Date().getTime() + 864000000))
  },
  loadUser ({ commit, dispatch }) {
    api().get('/user/me', {}).then(res => {
      dispatch('setUserProperties', res)
    })
  },
  editUser ({ commit }, userData) {
    return api().put('/user/edit', {
      birthDate: userData.birthDate,
      highestEducation: userData.highestEducation,
      job: userData.job,
      profileDescription: userData.profileDescription,
      skillList: userData.skillList.map(a => a.value ? a.value : a),
      googlePlaceId: userData.googlePlaceId,
      googlePlaceName: userData.googlePlaceName,
      intervalPayloadList: userData.intervalList
    })
  },
  editUserInterval ({ commit }, intervalData) {
    return api().put('/interval-edit', {
      intervalPayloadList: intervalData.intervalList
    })
  },
  setLocation ({ commit }, location) {
    commit('SET_LOCATION', location)
  },
  logoutUser ({ commit }) {
    commit('RESET_USER_STATE')
  },
  setUserProperties ({ commit }, res) {
    commit('SET_NAME', res.data.name)
    commit('SET_EMAIL', res.data.email)
    commit('SET_IMAGE_URL', res.data.imageUrl)
    commit('SET_BIRTH_DATE', res.data.birthDate)
    commit('SET_HIGHEST_EDUCATION', res.data.highestEducation)
    commit('SET_JOB', res.data.job)
    commit('SET_PROFILE_DESCRIPTION', res.data.profileDescription)
    commit('SET_SELECTED_SKILLS', res.data.skillList ? res.data.skillList.map(s => ({
      name: i18n.t(s),
      value: s
    })) : null)
    commit('SET_LOCATION', res.data.locationPayload)
    commit('SET_GOOGLE_PLACE_ID', res.data.googlePlaceId)
    commit('SET_GOOGLE_PLACE_NAME', res.data.googlePlaceName)
    commit('SET_INTERVALS', res.data.intervalPayloadList)
  },
  setCurrentLocation ({ commit }, currentLocation) {
    commit('SET_CURRENT_LOCATION', currentLocation)
  },
  setUserNotificationsCount ({ commit }) {
    api().get('/notification-count').then((res) => {
      commit('SET_NOTIFICATIONS_COUNT', res.data)
    })
  },
  resetUserNotificationsCount ({ commit }) {
    api().put('/notifications-seen').then((res) => {
      commit('SET_NOTIFICATIONS_COUNT', 0)
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
