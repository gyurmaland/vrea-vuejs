import api from '../../service/api'

const getDefaultState = () => {
  return {
    alertStash: [
      {}
    ]
  }
}

// initial state
const state = getDefaultState()

const mutations = {
  SET_ALERT_STASH (state, alertStash) {
    state.alertStash = alertStash
  },
  ADD_TO_ALERT_STASH (state, alert) {
    state.alertStash.push(alert)
  },
  REMOVE_FROM_ALERT_STASH (state, connectedWorkId) {
    state.alertStash = state.alertStash.filter(a => a.connectedWorkId !== connectedWorkId)
  }
}

const actions = {
  addToAlertStash ({ commit }, alert) {
    commit('ADD_TO_ALERT_STASH', alert)
  },
  removeFromAlertStash ({ commit }, connectedId) {
    commit('REMOVE_FROM_ALERT_STASH', connectedId)
  },
  getAllActiveAlertsFromUser ({ commit }) {
    api().get('/notifications/list').then((res) => {
      commit('SET_ALERT_STASH', res.data)
    })
  },
  setAlertsToSeenByConnectedId ({ commit }, connectedId) {
    return api().put('/notifications-seen/' + connectedId)
  },
  getAllActiveAlertsFromUserFilteredByWorkStatus ({ commit, state }, workStatus) {
    let notificationType
    switch (workStatus) {
      case 'PENDING':
        notificationType = 'NEW_WORK_REQUEST'
        break
      case 'WORK_ACCEPTED':
        notificationType = 'WORK_REQUEST_ACCEPTED'
        break
      case 'WORK_DONE':
        // code block
        break
      case 'WORK_DONE_AND_REVIEWED':
        notificationType = 'NEW_REVIEW'
        break
      case 'WORK_DENIED':
        notificationType = 'WORK_REQUEST_DENIED'
        break
      case 'WORK_CLOSED':
        // code block
        break
      default:
      // code block
    }
    return state.alertStash.filter(a => a.notificationType === notificationType).length
  },
  getAllActiveMsgAlertFromUser ({ commit, state }) {
    return state.alertStash.filter(a => a.notificationType === 'NEW_MESSAGE').length
  }
}

export default {
  state,
  mutations,
  actions
}
