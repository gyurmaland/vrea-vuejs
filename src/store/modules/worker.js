import api from '../../service/api'

const getDefaultState = () => {
  return {
    workerId: null,
    workerName: '',
    workerEmail: '',
    workerImageUrl: '',
    workerSkillList: [],
    workerGooglePlaceName: '',
    workerIntervals: []
  }
}

// initial state
const state = getDefaultState()

const getters = {}

const mutations = {
  SET_WORKER_ID (state, workerId) {
    state.workerId = workerId
  },
  SET_WORKER_NAME (state, workerName) {
    state.workerName = workerName
  },
  SET_WORKER_EMAIL (state, workerEmail) {
    state.workerEmail = workerEmail
  },
  SET_WORKER_IMAGE_URL (state, workerImageUrl) {
    state.workerImageUrl = workerImageUrl
  },
  SET_WORKER_SKILL_LlIST (state, workerSkillList) {
    state.workerSkillList = workerSkillList
  },
  SET_WORKER_GOOGLE_PLACE_NAME (state, workerGooglePlaceName) {
    state.workerGooglePlaceName = workerGooglePlaceName
  },
  SET_WORKER_INTERVALS (state, workerIntervals) {
    state.workerIntervals = workerIntervals
  },
  RESET_WORKER_STATE (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  getWorkerById ({ commit }, workerId) {
    api().get('/user/' + workerId, {}).then(res => {
      commit('SET_WORKER_ID', res.data.id)
      commit('SET_WORKER_NAME', res.data.name)
      commit('SET_WORKER_EMAIL', res.data.email)
      commit('SET_WORKER_IMAGE_URL', res.data.imageUrl)
      commit('SET_WORKER_SKILL_LlIST', res.data.skillList)
      commit('SET_WORKER_GOOGLE_PLACE_NAME', res.data.googlePlaceName)
      commit('SET_WORKER_INTERVALS', res.data.intervalPayloadList)
    })
  },
  findWorkers ({ commit }, searchData) {
    return api().post('/workers', {
      pagingRequest: {
        pageSize: searchData.pagingRequest.pageSize,
        page: searchData.pagingRequest.page
      },
      userSearchRequest: {
        location: searchData.userSearchRequest.location,
        workDateTime: searchData.userSearchRequest.searchDate,
        userListOrderBy: searchData.userSearchRequest.userListOrderBy,
        skill: searchData.userSearchRequest.skill
      }
    })
  },
  resetWorker ({ commit }, accepted) {
    commit('RESET_WORKER_STATE', accepted)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
