import api from '../../service/api'

const getDefaultState = () => {
  return {
  }
}

// initial state
const state = getDefaultState()

const mutations = {
}

const actions = {
  sendMessage ({ commit }, messageData) {
    return api().post('/message/save', {
      workId: messageData.workId,
      senderName: messageData.senderName,
      text: messageData.text
    })
  },
  findMessages ({ commit }, searchData) {
    return api().post('/messages', {
      pagingRequest: {
        pageSize: searchData.pagingRequest.pageSize,
        page: searchData.pagingRequest.page
      },
      messageSearchRequest: {
        workId: searchData.messageSearchRequest.workId
      }
    })
  }
}

export default {
  state,
  mutations,
  actions
}
