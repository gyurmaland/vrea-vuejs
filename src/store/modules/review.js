import api from '../../service/api'

const getDefaultState = () => {
  return {

  }
}

// initial state
const state = getDefaultState()

const mutations = {
}

const actions = {
  getWorkerRating ({ commit }, workerId) {
    return api().get('/review/worker-rating-avg/' + workerId)
  },
  saveReview ({ commit, state }, reviewData) {
    return api().post('/review/save', {
      rating: reviewData.rating,
      ratingText: reviewData.ratingText,
      workId: reviewData.workId
    })
  },
  findReviews ({ commit }, searchData) {
    return api().post('/reviews', {
      pagingRequest: {
        pageSize: searchData.pagingRequest.pageSize,
        page: searchData.pagingRequest.page
      },
      reviewSearchRequest: {
        userId: searchData.reviewSearchRequest.userId
      }
    })
  }
}

export default {
  state,
  mutations,
  actions
}
