export const calculateWorkStatus = {
  methods: {
    getWorkStatus () {
      if (this.workRequestData.accepted === false) {
        if (this.$dayjs((this.workRequestData.intervalPayload.end)).isBefore(this.$dayjs(new Date()))) {
          return 'WORK_CLOSED'
        } else {
          return 'WORK_DENIED'
        }
      } else if (this.workRequestData.accepted === true) {
        if (this.$dayjs((this.workRequestData.intervalPayload.end)).isBefore(this.$dayjs(new Date()))) {
          return 'WORK_FINISHED'
        } else {
          return 'WORK_ACCEPTED'
        }
      } else {
        if (this.$dayjs((this.workRequestData.intervalPayload.end)).isBefore(this.$dayjs(new Date()))) {
          return 'WORK_CLOSED'
        } else {
          return 'WORK_PENDING'
        }
      }
    }
  }
}
