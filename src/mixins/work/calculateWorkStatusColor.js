export const calculateWorkStatusColor = {
  methods: {
    getWorkStatusColor () {
      if (this.workRequestData.accepted === false) {
        return '#D32F2F'
      } else if (this.workRequestData.accepted === true) {
        return '#388E3C'
      } else {
        return '#F57C00'
      }
    }
  }
}
